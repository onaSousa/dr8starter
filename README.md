
# DR8Starter

A basic front-end starter theme for Drupal 8.x focused in CSS, JS and Twig, using Gulp with task manager.

**Authors:** Jonas Sousa

#### Installation

Download and extract the theme package in your sites/all/themes/custom directory. As an admin, go to Administration > Appearance to enable the theme.
